import server from "./../../main";
import chai from "chai";
import chaiHttp from "chai-http";

const expect = chai.expect;

chai.use(chaiHttp);
const token =
  "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJPbmxpbmUgSldUIEJ1aWxkZXIiLCJpYXQiOjE2MjIyNzU1MzksImV4cCI6MTY1MzgxMTUzOSwiYXVkIjoid3d3LmV4YW1wbGUuY29tIiwic3ViIjoianJvY2tldEBleGFtcGxlLmNvbSIsImlkIjoiMSJ9.LGLagPpG8ROzTiHL80ZqwjX2kR238jGpVSgzbCb60oo";

describe("pet api", () => {
  it("can return 401 by not authen", (done) => {
    chai
      .request(server)
      .get("/user/getAll")
      .set("authorization", "dada")
      .end((err, res) => {
        expect(res).to.have.status(401);
      });
    done();
  });
  it("can get all pet", (done) => {
    chai
      .request(server)
      .get("/pet/getAll")
      .set("authorization", token)
      .end((err, res) => {
        expect(res).to.have.status(200);
        expect(res.text).equal(
          JSON.stringify([
            { id: "1", name: "pest1" },
            { id: "2", name: "pest2" },
            { id: "3", name: "pest3" },
          ])
        );
      });
    done();
  });
  it("can get pet by id", (done) => {
    chai
      .request(server)
      .get("/pet/1")
      .set("authorization", token)
      .end((req, res) => {
        expect(res).to.have.status(200);
        expect(res.text).equal(JSON.stringify({ id: "1", name: "pest1" }));
      });
    done();
  });
  it("can return 400 by wrong request", (done) => {
    chai
      .request(server)
      .get("/pet/a")
      .set("authorization", token)
      .end((req, res) => {
        expect(res).to.have.status(400);
      });
    done();
  });
});
