import server from "./../../main";
import chai from "chai";
import chaiHttp from "chai-http";

const expect = chai.expect;

chai.use(chaiHttp);
const token =
  "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJPbmxpbmUgSldUIEJ1aWxkZXIiLCJpYXQiOjE2MjIyNzU1MzksImV4cCI6MTY1MzgxMTUzOSwiYXVkIjoid3d3LmV4YW1wbGUuY29tIiwic3ViIjoianJvY2tldEBleGFtcGxlLmNvbSIsImlkIjoiMSJ9.LGLagPpG8ROzTiHL80ZqwjX2kR238jGpVSgzbCb60oo";

describe("user api", () => {
  it("can return 401 by not authen", (done) => {
    chai
      .request(server)
      .get("/user/getAll")
      .set("authorization", "1234")
      .end((err, res) => {
        expect(res).to.have.status(401);
      });
    done();
  });
  it("can get all user", (done) => {
    chai
      .request(server)
      .get("/user/getAll")
      .set("authorization", token)
      .end((err, res) => {
        expect(res).to.have.status(200);
        expect(res.text).equal(
          JSON.stringify([
            { id: "1", username: "user1", password: "1234" },
            { id: "2", username: "user2", password: "4567" },
            { id: "3", username: "user3", password: "6789" },
          ])
        );
      });
    done();
  });
  it("can get user by id", (done) => {
    chai
      .request(server)
      .get("/user/1")
      .set("authorization", token)
      .end((req, res) => {
        expect(res).to.have.status(200);
        expect(res.text).equal(
          JSON.stringify({ id: "1", username: "user1", password: "1234" })
        );
      });
    done();
  });
  it("can return 400 by wrong request", (done) => {
    chai
      .request(server)
      .get("/user/a")
      .set("authorization", token)
      .end((req, res) => {
        expect(res).to.have.status(400);
      });
    done();
  });
});
