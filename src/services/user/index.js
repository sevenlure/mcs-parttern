import BaseBuilder from "./../base";
import { Users } from "./../../models/user";
import { filter, indexOf } from "lodash";
import Boom from "@hapi/boom";

const save = (user) => {
  const { id, username, password } = user;
  if (!id && !username && !password) {
    throw Boom.badImplementation("user invalid");
  }
  User.push(user);
  return { err: null, result: user };
};

const update = (id, user) => {
  const oldUser = find((user) => user.id == id)(Users);
  if (!user) throw Boom.badImplementation("user not found");
  const index = indexOf(oldUser);
  const updateUser = { ...oldUser, ...user };
  Users.fill(updateUser, index, index + 1);
  return { err: null, result: updateUser };
};

const deleteOne = (id) => {
  let roweffected = Users.length;
  Users = filter((user) => user.id != id)(Users);
  roweffected = roweffected - ListData.length;
  return { err: null, roweffected };
};

const userService = {
  save,
  update,
  deleteOne,
  ...BaseBuilder(Users),
};

export default userService;
