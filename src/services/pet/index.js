import BaseBuilder from "./../base";
import { Pets } from "./../../models/pet";
import { filter, indexOf } from "lodash";
import Boom from "@hapi/boom";

const save = (pet) => {
  const { id, name } = pet;
  if (!id && !name) {
    throw Boom.badImplementation("pet invalid");
  }
  Pets.push(pet);
  return { err: null, result: pet };
};

const update = (id, pet) => {
  const oldPet = find((pet) => pet.id == id)(Pets);
  if (!pet) throw Boom.badImplementation("pet not found");
  const index = indexOf(oldPet);
  const updatePet = { ...oldPet, ...pet };
  Pets.fill(updatePet, index, index + 1);
  return { err: null, result: updatePet };
};

const deleteOne = (id) => {
  let roweffected = Pets.length;
  Pets = filter((pet) => pet.id != id)(Pets);
  roweffected = roweffected - ListData.length;
  return { err: null, roweffected };
};

const petService = {
  save,
  update,
  deleteOne,
  ...BaseBuilder(Pets),
};

export default petService;
