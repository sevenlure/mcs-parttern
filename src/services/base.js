import { isEmpty } from "lodash";
import { find } from "lodash/fp";

const findAll = (Model) => () => {
  return { err: null, result: Model };
};

const findById = (Model) => (id) => {
  if (isEmpty(id)) {
    return { err: null, result: {} };
  }
  const itemMactchByIdHandler = (item) => item.id === id;
  const result = find(itemMactchByIdHandler)(Model);
  return { err: null, result: isEmpty(result) ? {} : result };
};

const BaseBuilder = (Model) => {
  return {
    findAll: findAll(Model),
    findById: findById(Model),
  };
};

export default BaseBuilder;
