import { verify } from "jsonwebtoken";

export const ensureENV = (key) => {
  if (!process.env[key]) {
    console.log(`Error: Please specify ${key} in environment`);
    process.exit(1);
  }
};

export const verifyToken = (token) => {
  try {
    const decoded = verify(token, process.env.AUTH_SECRET);
    return decoded;
  } catch (err) {
    console.log(err);
    return {};
  }
};
