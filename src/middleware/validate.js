export const VALIDATE_TYPE_MAP = {
  PARAMS: "PARAMS",
  BODY: "BODY",
  QUERY: "QUERY",
};

const validateMiddleware = (schema, type) => async (req, res, next) => {
  try {
    let data = {};

    if (type == VALIDATE_TYPE_MAP['PARAMS']) data = req.params;
    if (type == VALIDATE_TYPE_MAP['BODY']) data = req.body;
    if (type == VALIDATE_TYPE_MAP['QUERY']) data = req.query;

    const { error } = schema.validate(data);
    const valid = error == null;
    if (valid) {
      next();
    } else {
      res.status(400).json({ error: "request không hợp lệ" });
    }
  } catch (err) {
    console.log({ err });
  }
};

export default validateMiddleware;