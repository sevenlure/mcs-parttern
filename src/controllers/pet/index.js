import petService from "./../../services/pet";
import Boom from "@hapi/boom";
import { baseCtl } from "./../base";

const save = (req, res, next) => {
  try {
    const data = req.body;
    const { err, result } = petService.save(data);
    if (err) {
      throw Boom.badRequest(err);
    }
    res.status(200).send(result)
  } catch (err) {
    next(err);
  }
};

export const petCtl = {
  save,
  ...baseCtl(petService),
};
