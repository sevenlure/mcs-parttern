import Joi from "joi";

const getByIdSchema = Joi.object({
  id: Joi.string()
    .regex(/^[0-9]*$/)
    .required(),
});

const saveSchema = Joi.object({
  id: Joi.string(),
  username: Joi.string(),
  password: Joi.string(),
});

export const validateSchema = {
    getByIdSchema,
    saveSchema
}