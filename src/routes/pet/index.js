import express from "express";
import validateMiddleware, {
  VALIDATE_TYPE_MAP,
} from "./../../middleware/validate";
import { petCtl } from "./../../controllers/pet";
import { validateSchema } from "./validate";

const router = express.Router();

router.get("/getAll", petCtl.getAll);
router.get(
  "/:id",
  validateMiddleware(validateSchema.getByIdSchema, VALIDATE_TYPE_MAP.PARAMS),
  petCtl.getById
);
router.post(
  "/",
  validateMiddleware(validateSchema.saveSchema, VALIDATE_TYPE_MAP.BODY),
  petCtl.save
);

export default router;
